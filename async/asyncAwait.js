async function hola(nombre) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Hola,', nombre);
      resolve(nombre);
    }, 1000);
  });
  
}

async function hablar(nombre) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Bla bla bla bla bla...');
      resolve(nombre);
    }, 500);
  })
}

async function adios(nombre) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Adios,', nombre);
      resolve();
    }, 1000)
  })
}
 

// ---


async function main() {
  let nombre = await hola('Miguel');
  await hablar();
  await hablar();
  await hablar();
  await hablar();
  await adios(nombre);
  console.log('Terminando el proceso 1');

}

console.log('Iniciando el proceso...');
main();
console.log('Terminando el proceso 1'); // AQUI SE PUEDE OBSERVAR 

