function hola(nombre) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Hola,', nombre);
      resolve(nombre);
    }, 1000);
  });
  
}

function hablar(nombre) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Bla bla bla bla bla...');
      // resolve(nombre);
      reject('Hay un error');
    }, 500);
  })
}

function adios(nombre) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Adios,', nombre);
      resolve();
    }, 1000)
  })
}
 

// ---

console.log('Iniciando el proceso...');
hola('Miguel')
  // COMO EL PARAMETRO ES EL MISMO ENTONCES SE PUEDE HACER UN LLAMADO DIRECTO
  // .then(nombre => {
  //   return adios(nombre)
  // })
  // .then((nombre) => {
  //   console.log('Terminado el proceso');
  // })
  .then(hablar)
  .then(hablar)
  .then(hablar)
  .then(adios)
  .then((nombre) => {
    console.log('Terminado el proceso');
  })
  .catch(error => {
    console.error('Ha habido un error:');
    console.error(error);
  })