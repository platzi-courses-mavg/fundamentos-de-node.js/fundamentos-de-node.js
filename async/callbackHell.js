function hola(nombre, miCallback) {
  setTimeout(function() {
    console.log('Hola, ' + nombre);
    miCallback(nombre);
  }, 2000);
}

function hablar(callbackHablar) {
  setTimeout(function() {
    console.log('Bla bla bla bla bla...');
    callbackHablar();
  }, 500);
}

function adios(nombre, otroCallback) {
  setTimeout(() => {
    console.log('Adios,', nombre);
    otroCallback();
  }, 1000)
}

function conversacion(nombre, veces, callback) {
  // Usando RECURSIVIDAD, que es simplemente llamar a la misma función pero con información ligeramente distanta
  if (veces > 0) {
    hablar(function() {
      conversacion(nombre, --veces, callback);
    })
  } else {
    adios(nombre, callback);
  }
}

// ---

console.log('Iniciando proceso...');

hola('Miguel', function(nombre) {
  conversacion(nombre, 3, function() {
    console.log('Terminando proceso...');
  });
});

// --- CALLBACK HELL
// hola('Miguel', function(nombre) {
//   hablar(function() {
//     hablar(function() {
//       hablar(function() {
//         hablar(function() {
//           adios(nombre, function() {
//             console.log('Terminando proceso...'); 
//           });
//         });
//       });
//     });
//   });
// });