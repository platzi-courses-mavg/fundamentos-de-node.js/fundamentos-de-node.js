// --- ASINCRONISMO
/*
function soyAsincrona() {
  // console.log("Hola, soy una función asícncrona");
  setTimeout(function() {
    console.log('Estoy siendo asíncrona');
  }, 1000); // Que s eejecute en 1000ms = 1s
}

console.log('Iniciando proceso...');

soyAsincrona();

console.log('Terminando proceso...');
*/


// --- CALLBACKS
/*
function soyAsincrona(miCallback) {
  setTimeout(function() {  // DESDE AQUÍ YA SE UTILIZA UN CALLBACK, LLAMANDO A UNA FUNCIÓN DENTRO DEL 'setTimeout
    console.log('Estoy siendo asíncrona');
    miCallback();
  }, 1000);
}

console.log('Iniciando proceso...');

soyAsincrona(function() {
  console.log('Terminando proceso...');
});
*/

function hola(nombre, miCallback) {
  setTimeout(function() {
    console.log('Hola, ' + nombre);
    miCallback(nombre);
  // }, 1000);
  }, 2000);
}

function adios(nombre, otroCallback) {
  setTimeout(() => {
    console.log('Adios,', nombre);
    otroCallback();
  // }, 2000)
  }, 1000)
}

console.log('Iniciando proceso...');

// hola('Miguel', function() {
  // adios('Miguel', function() {
hola('Miguel', function(nombre) {
  adios(nombre, function() {
    console.log('Terminando proceso...'); 
  })
});