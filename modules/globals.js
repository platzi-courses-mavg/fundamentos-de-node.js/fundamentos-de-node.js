// console.log(global);
// console.log(setInterval);

// ---
/* setInterval. Es una función muy utilizada que se ejecuta siempre y después 
  del intervalo de tiempo especificado. Por ejemplo se puede utilizar al 
  intentar establecer una conexión a una Base de datos, queremos aseguranos 
  que se haga así que decidimos reintentar entrablar la conexión cada 5s, y 
  estar monitoreando, si en 5 intentos no se ha logrado, puede entonces, que 
  el servidor este caído.
*/
let i = 0;
let interval = setInterval(() => {
  console.log('Hola');
  if (i === 3 )
    clearInterval(interval);
  i++;
}, 1000)

// ---
/* setImmediate. Función similar a setInterval solo que se ejecuta una sola 
  vez pero al instante.
*/
setImmediate(() => {
  console.log('Hola');
})

// ---
console.log(process);
console.log(__dirname);
console.log(__filename);

global.miVar = 'Valor'
console.log(miVar);
