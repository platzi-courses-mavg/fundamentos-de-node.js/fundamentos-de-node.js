// ---
console.log('Algo');

// ---
console.info('Algo');

// ---
console.warn('Algo');

// ---
console.error('Algo');

// ---
var tabla = [
  {
    a: 1,
    b: 'Z'
  },
  {
    a: 2,
    b: 'Y'
  }
]
console.table(tabla);

// ---
// console.log('Conversación:'); 
console.group('Conversación:');
console.log('Hola');
console.group('Charla:')
console.log('Blablabla');
console.log('Blablabla');
console.log('Blablabla');
console.groupEnd();
console.log('Adios');
// console.groupEnd('Conversación:');
console.groupEnd();
console.log('Otra cosa');

function f1() {
  console.group('Función 1');
  console.log('f1');
  console.log('f1');
  f2();
  console.log('De vuelta en f1');
  // console.groupEnd('Función 1');
  console.groupEnd();
}

function f2() {
  console.group('Función 2');
  console.log('f2');
  console.log('f2');
  // console.groupEnd('Función 2');
  console.groupEnd();
}

console.log('Iniciamos');
f1();

// ---
console.count('veces');
console.count('veces');
console.count('veces');
console.countReset('veces');
console.count('veces');
console.count('veces');