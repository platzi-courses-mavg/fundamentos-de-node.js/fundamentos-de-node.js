const { exec, spawn } = require('child_process');
// const exce = require('child_process').exec;

// exec('ls -la', (err, stdout, sterr) => {
exec('dir', (err, stdout, sterr) => {
  if (err) {
    console.error(err);;
    return false;
  }

  console.log(stdout);
})

exec('node ./console.js', (err, stdout, sterr) => {
  if (err) {
    console.error(err);;
    return false;
  }

  console.log(stdout);
})

// let proceso = spawn('ls', ['-la']);
let proceso = spawn('cmd.exe', ['/c','dir']);

console.log(proceso.pid);
console.log(proceso.connected);

// ORIENTACIÓN A EVENTOS
proceso.stdout.on('data', function(dato){
  console.log(proceso.killed);
  console.log(dato.toString());
})

proceso.on('exit', function() {
  console.log('El proceso termino.');
  console.log(proceso.killed);
})
