const fs = require('fs');

function leer(ruta, cb) {
  fs.readFile(ruta, (err, data) => {
    // Archivo ya leído
    // console.log(data.toString());
    cb(data.toString());
  })
}

// leer(__dirname + '/file.txt');
leer(__dirname + '/file.txt', console.log);

function escribir(ruta, contenido, cb) {
  fs.writeFile(ruta, contenido, (err, data) => {
    if (err)
      console.error('No he podido escribir en el archivo:', ruta);
    else
      console.log('Éxito al escribir');
  })
}

escribir(__dirname + '/file1.txt', 'Soy un archivo nuevo', console.log)
leer(__dirname + '/file1.txt', console.log);

function borrar(ruta, cb) {
  fs.unlink(ruta, cb);
}

borrar(__dirname + '/file1.txt', console.log);

