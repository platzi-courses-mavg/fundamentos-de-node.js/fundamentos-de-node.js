// const p = require('process');

process.on('beforeExit', () => {
  console.log('El proceso va a terminar...');
});

process.on('exit', () => {
  console.log('Vale, el proceso terminó');
  setTimeout(() => {
    console.info('Esto nunca se verá debido a que es un proceso asíncrono y no puede ser ejecutado después del "exit"');
  }, 0);
});

process.on('uncaughtException', (err, origin) => {
  console.error('Olvidamos capturar un error');
  // console.error(err);
  setTimeout(() => {
    console.info('Esto si se verá debido a que es un proceso previo al "exit"');
  }, 0);
})

funcionQueNoExiste();
console.log('Si el error no se recoge, este mensaje no se muetra!');


// process.on('uncaughtRejection')