# Fundamentos de Node <a name="inicio"/>

## Contenido
+ 1. [Node: orígenes y filosofía](#Node)
+ 2. [Event Loop: asincronía por diseño](#Event-Loop)
+ 3. [Monohilo: implicaciones en diseño y seguridad](#Monohilo)
+ 4. [Variables de Entorno](#Variables-de-Entorno)
+ 5. [Nodemon y pm2](#Nodemon-y-pm2)
+ 6. [Asincronía](#Asincronia)
  + 6.1 [Callbacks](#Callbacks)
  + 6.2 [Promesas](#Promesas)
  + 6.3 [Async Await](#Async)
+ 7. [Módulos del Core](#ModulosCore)
  + 7.1 [Globals](#Globals)
  + 7.2 [File System](#Fs)
  + 7.3 [Console](#Console)
  + 7.4 [Try / Catch](#TryCatch)
  + 7.5 [Procesos Hijo](#Procesos-hijo)
  + 7.6 [Módulos nativos en C++](#C++)
  + 7.7 [HTTP](#HTTP)
  + 7.8 [Sistema Operativo (OS)](#OS)
  + 7.9 [Process](#Process)
+ 8. [Módulos y paquetes externos](#ModulosExternos)
  + 8.1 [Gestión de paquetes: NPM y package.json](#NPM)
  + 8.2 [Construyendo módulos: Require e Import](#Module)
  + 8.3 [Módulos útiles](#Modulos-utiles)
  + 8.4 [Datos almacenados vs en memoria (Buffer y Stream)](#Datos)
+ 9. [Trucos](#Trucos)
  + 9.1 [Temporizador de procesos (*Benchmark*)](#Benchmark)
  + 9.2 [Debugger](#Debugger)
  + 9.3 [Error First Callbacks](#ErrorCallbacks)
+ 10. [Manejo de Herramientas](#Herramientas)
  + 10.1 [Scraping (*Puppeteer*)](#Scraping)
  + 10.2 [Automatización de Procesos](#Automatizacion)
  + 10.3 [Aplicaciones de Escritorio](#Aplicaciones)


---
## Node: orígenes y filosofía <a name="Node"/>[↑](#inicio)
### ¿Qué es NodeJS?
Un **entorno de ejecución** de JavaScript fuera del navegador, permitiendo así ejecutar Java Script sin la necesidad de algún anvegador como Google Chrome, Firebox, etc., puede ser ejecutado en servidores, o cualquier dispositivo y para la creación de Herramientas, Transpiladores, screping, automatización, etc.

Fue creado en 2009 y **se orienta a servidores**.

Corre sobre el **Motor V8**, entorno de ejecución de JS libre (open source), escrito en C++ y creado por Google desde el 2008.
Es super potente, debido a su principal diferencia con otros entornos de JS que interpretan el código línea por línea, el motor V8, en cambio, convierte el código JS a código maquina de manera rápida, permitiendo así que las aplicaciones fallen desde el inicio si tienen algún error de sintaxis, y no hasta el momento de lectura de esa línea en específico.

Todo funciona en base a **Módulos**, todo lo que no sea sintaxis de programación, son módulos, son simplemente pequeñas piezas de código, donde algunas vienen por defecto en el mismo Node y otras no, pero pueden ser "recogidas" desde gestores de paquetes. 
Muchos módulos vienen por defecto en el paquete de Node. Se permite la creación de módulos propios.

A diferencia de otros lenguajes, NodeJS está **orientado a eventos**, es decir, se tiene un bucle de eventos ejecutandose constantemente y en algún momento se dispararán eventos para ser escuchados, logrando así una **programación reactiva**, en muy simples palabras, la orientación a eventos permite decir "cuando suceda 'esta cosa' que se ejecute mi código"

### Bases de JavaScript
Es un lenguaje **concurrente**, aunque es **monohilo todas sus entradas y salidas son asíncronas**, de esta manera se tendrá una única tarea corriendo en cada núcleo del procesador, programando así todas las peticiones de manera asíncrona para evitar "quedarse bloqueante", pemitiendo tener muchas conexiones para todo lo que tiene que ver con entrada-salida, con gestión de archivos, servidores SCP, etc.

### Documentación:
[Web Oficial](https://nodejs.org/es/)


---
## Event Loop: asincronía por diseño <a name="Event-Loop"/>[↑](#inicio)
El Event Loop (Bucle de eventos), es simplemente un bucle que gestiona, de forma asíncrona, todos los eventos de la aplicación. 

En otras palabras, es un bucle que todo el tiempo está ejecutandose y básicamente hay un proceso gestionandolo de forma automática, ejecutando así todos los procesos envíados en forma asíncrona, evitando su bloqueo y permitiendo la recepción de nuevas peticiones, *por esto NodeJS es altamente concurrente*.

### Flujo
**El Event Queue (Cola de eventos)** contiene todos los eventos que se ejecuten en el código, cada línea genera eventos, *una función, una expresion, una petición, etc.*, todos estos eventos se "encolan" en nuestro **Event Queue** y se van enviando uno a uno hacía el **Event Loop**, donde serán procesados, es decir, si se pueden resolver dichos eventos de manera rápida, entonces serán resueltos, de lo contrario, a los eventos donde su ejecución no sea muy rápida, serán envíados al **Thread Pool**, y aquí es donde se comenzará a gestionar de forma asíncrona todo, levantando un nuevo thread (hilo) dentro del procesador para cada petición a efectuar, encargandose automaticamente de la ejecución de los procesos sin importar el tiempo de finalización del evento, esto quiere decir, por ejemplo, que *las consultas a Bases de Datos, operaciones muy lentas, lecturas de archivos, etc.*, no estarán bloqueando el *hilo principal*, el propio **Event Loop** seguirá funcionando como si nada, y cada hilo, creado en el **Thread Pool**, se encargará de gestionar su propio evento por más lento que este sea. Una vez procesados dichos eventos, serán devueltos al **Event Loop**, donde se evaluará si es necesario regresarlos también al **Event Queue** para activar nuevos eventos que pudierán estar a la espera de respuestas de los eventos previos, reiniciando así el ciclo del proceso al enviar estos nuevos eventos al **Event Loop**.

El **Motor V8** es el responsable de que todo esto funcione.

Esto es el concepto clave para diferenciar NodeJS de otros lenguajes de servidor como PHP o Java que por defecto son síncronos, donde una operación va siempre detras de otra. Así es como NodeJS permite ejecutar en paralelo diferentes tareas, mejorando considerablemente la eficiencia.

Para aclarar el proceso del **Event Loop**, la mejor forma de verlo es como un círculo que va girando, donde los eventos pasan del **Event Queue** al **Evento Loop** y ahí serán evaluados revisando si estos tomarán poco tiempo de ejecución, de ser así serán ejecutados al instante, de lo contrario, pasarán el **Thread Pool**, donde este administrará la ejecución paralela de estos eventos creando un proceso para cada uno, al terminar su ejecución, dichos eventos son devueltos al **Event Loop**, donde se revisará si es necesario retornar algunos de los eventos debido a que el **Event Queue** puede estar en espera de los mismos para generar una función nueva y enviarla al **Event Loop**, iniciando así el ciclo nuevamente.

#### En resumen se puede decir que:
##### Event Queue: 
Contiene todos los eventos que se generan por nuestro código (Funciones, peticiones, etc.), estos eventos quedan en una cola que van pasando uno a uno al Event Loop.

##### Event Loop: 
Se encarga de resolver los eventos ultra rápidos que llegan desde el Event Queue. En caso de no poder resolverse rápido, enviá el evento al Thread Pool.

##### Thread Pool: 
Se encarga de gestionar los eventos de forma asíncrona. Una vez terminado lo devuelve al Event Loop, donde evaluará si lo pasa a Event Queue o no.

### Material de apoyo:
* [The Node.js Event Loop, Timers, and `process.nextTick()`.]( https://nodejs.org/uk/docs/guides/event-loop-timers-and-nexttick/)


---
## Monohilo: implicaciones en diseño y seguridad <a name="Monohilo"/>[↑](#inicio)
Los hilos son generados a partir de la creación de un proceso, en otras palabras, un proceso es un hilo de ejecución, conocido como **Monohilo**.

**¿Qué pasa al ejecutar código con el comando `node`?**
Lo primero en suceder es que se abre un nuevo proceso de Node que interpretará todo el archivo, lo convierte en código máquina y prepara todo lo necesario para la "corrida", para por último ejecutarlo; una vez ejecutado el proceso, este se cierra.

Una desventaja del monohilo es al ocurrir un error, ya que esto termina por completo todo el proceso. Por eso es clave estar al pendiente de todos los errores que podrían detener la ejecución del proyecto.

[Código](./monohilo.js)


---
## Variables de Entorno <a name="Variables-de-Entorno"/>[↑](#inicio)
Se utilizan para determinar valores que no se deben definir dentro del software, por ejemplo, valores para hacer peticiones algún API, una clave, un token, accesos y credenciales, etc. **No deberían estar en el código, porque el código NO debe comprometer información tan delicada**.

### Declaración
Se pueden definir las variables de entorno justo antes de la ejecución del código, agregandolas al principio del comando de ejecución, por ejemplo: 

Consola | Comando
--------|--------
Linux | `NOMBRE=Miguel node variablesEntorno.js` **NOTA:** Para declarar/utilizar más de una variable basta con agregarlas al inicio seguidas de un espacio. Ejemplo: `N1=Miguel N1=Angel node variablesEntorno.js`
PowerShell | `$env:NOMBRE='Miguel'; node variablesEntorno.js`
CMD | `setx NOMBRE Miguel` y `node variablesEntorno.js` **NOTA:** Es necesario reiniciar la consola (o incluso el editor de código) para visualizar los cambios.

En Windows también se pueden agregar las variables de entorno mediante su interface de la siguiente manera:
* Desde *Este equipo* en el *Explorador de Archivos (tecla Windows (⊞ Win) + e)* dar click derecho y seleccionar *Propiedades*, esto abrirá el *Panel de Control*
* Entrar a *Configuración avanzada del sistema*
* Seleccionar *Variables de entorno...*, desde ahí se podrán administrar las Variables de Entorno del Sistema.

**NOTA:** Por buenas prácticas las variables de entorno siempre son declaradas en mayúscuslas y en *snake case*.

[Código](./variablesEntorno.js)


---
## Nodemon y pm2 <a name="Nodemon-y-pm2"/>[↑](#inicio)
### Nodemon
Es un gestor de NodeJS como si fuera un demonio (deamon). Un **deamon** en Linux, es un proceso en ejecución donde se pueden observar los cambios en tiempo real. Entonces nodemon lo que hace es crear una herramienta o utilidad, que cada vez que detecte cambios en el código o en las dependencias utilizadas en el mismo, vuelve a compilar y ejecutar ese código. Dichos cambios son detectados al guardarlos.

#### Instalación
Ejecutar el comando `npm install -g nodemon`. 

`-g` indica que la dependencia se instalará en el equipo de manera global, permitiendo ser utilizada en cualquier código dentro del sistema.

### PM2
Permite ejecutar la aplicación en Cluster, generar los logs en otro sitio, monitorear la aplicación, si hay algún error en código y se "rompe" el proceso, pm2 lo detecta y vuelve a ejecutarlo, etc.

**NOTA:** No utilizarlo en desarrollo, solo en producción, ya que en desarrollo se espera que el código cambie rápido y se muestren dichos cambios al momento.

#### Algunos Comandos
Comando | Descripción
--------|------------
`pm2 start app.js` | Inicia y "Daemoniza" la aplicación especificada
`pm2 start api.js -i 4` | Balancea 4 instancias de la aplicación especificada. Se puede utilizar por ejemplo al momento de querer ejecutar un API, dando sentido a crear diferentes instancias en Cluster, una por cada nucleo del procesador, por ejemplo, para tener balanceo de carga y una ejecución del sistema mucho más rápida.
`pm2 monitor` | Permite Monitorear las aplicaciones en producción.
`pm2 status` | Proyecta una tabla donde se especifica el estado de las aplicaciones ejecutadas.
`pm2 logs` | Muestra los logs que van ejecutandose en tiempo real.
`pm2 stop 'id del proceso'` o `pm2 stop 'nombre del proceso'` | Para o "mata" el proceso indicado.

#### Instalación
Ejecutar el comando `npm install pm2 -g`.

### Documentación:
* [Nodemon npm.](https://www.npmjs.com/package/nodemon)
* [PM2 npm.](https://www.npmjs.com/package/pm2)


---
## Asincronía <a name="Asincronia"/>[↑](#inicio)
### Callbacks <a name="Callbacks"/>[↑](#inicio)
**En JavaScript las funciones son elementos del primer nivel**, permitiendo así su uso de cualquier manera, por ejemplo, pasandolas como parámetro, esto último es un *Callback*.

Es posible anidar funciones Callbacks, agregandolas una dentro de otra, pero esto puede generar ciertos problemas durante la práctica, ya que depende del tiempo de ejecución de cada callback para saber cual finalizará primero y cuál después. Este detalle se puede contener enviando parámetros como variables entre los llamados a los callback, de esta manera no solamente podemos entender cuando van los callback seguidos, sino que además podemos asegurarnos de la correcta ejecución de la aplicación, así como de compartir información entre los callback del principio hasta el final.

[Código](./async/callback.js)

#### Callback Hell.
Infierno de los Callback, se refiere a la saturación de anidaciones de callbacks donde es muy difícil gestionar la sincronía.

Para evitarlo, lo primero es crear funciones, definiendolas al principio del código, lo segundo es que al empezar a trabajar con callbacks más complejos se deberán generar soluciones más complejas, como el uso de funciones recursivas (que es simplemente llamar a la misma función, pero con información ligeramente distanta), por ejemplo. 

Con esto se puede gestionar la complejidad de los callbacks, y además se obtienen funciones mucho más legibles y mucho más manejables.

[Código](./async/callbackHell.js)


---
### Promesas <a name="Promesas"/>[↑](#inicio)
Con el manejo de callbacks es muy sencillo caer en el *callback hell*. Para evitar esto se crea una nueva forma de trabajar con asicronía, las **promesas**. Con las promesas se decide cambiar el concepto de *"voy a ejecutar una función después"* por un *estado*, las promesas pueden estar resueltas, pueden estar pendientes o pueden fallar.

Las promesas son llamadas como una clase global que como parámetro tiene un callback que recibe otros dos parámetros *resolve* y *reject* (`new Promise((resolve, reject) => { }`).

La principal diferencia entre *promesas* y *callbacks*, es que las promesas se pueden ir aninando.

Cuando una promesa falla se ejecuta su "proceso" *reject*, estas fallas terminan la ejecución de nuestro código, por eso es importante estár siempre al pendiente de estas.
Es un problema típico y para atenderlo basta con agregar un `catch()` al final de nuestros llamados de promesas (después de los `then()`), esto envia el error directo al `catch()` y evita que los errores de las promesas se sigan propagando hacia otras promesas.

[Código](./async/promises.js)


---
### Async Await <a name="Async"/>[↑](#inicio)
La sintaxis de **Async await** permite definir una función de forma explicita, como función asíncrona y poder esperar a que esta función termine. En otras palabras, el código estará ejecutando funciones, una tras otra hasta encontrarse con una función asíncrona, aquí la ejecución "se detiene" queda en "espera" de finalizar esta función asíncrona (que se desea, sea una función síncrona), esto NO representa ningún problema, a nivel técnico no estará bloqueando el hilo principal, y este podrá seguir escuchando nuevas peticiones y todo este tipo de cosas.

La sintaxis de Async Await es un poco de lo que se le conoce como ***azúcar sintático***, donde la sintaxis de la programación facilita (abstrae) las cosas complejas de entender - leer.

Es común cometer el error de agregar `await` fuera de un `async`. `await` es solo valido dentro de una función `async`, por ejemplo, no se puede ejecutar un `await` que se encuentre en el cuerpo principal del código.

Las async await terminan siendo funciones síncronas que se ejecutan de manera asíncrona. Permite "convertir" procesos asíncronos en aparentemente síncronos pero la función nunca deja de ser asíncrona.

[Código](./async/asyncAwait.js)


---
## Módulos del Core <a name="ModulosCore"/>[↑](#inicio)
### Globals <a name="Globals"/>[↑](#inicio)
El objeto `global` proporciona variables y funciones que están disponibles en cualquier lugar. Por defecto, aquellas que están integradas en el lenguaje o el entorno.
Los módulos globales básicamente son muchos módulos que están incluidos en Node.js (al instalarlo), por ejemplo, `console.log`, `setTimeout`, etc.

Algunas de las funciones incluidas en las *globales* de Node.js son:
* `setInterval`. Es una función muy utilizada que se ejecuta siempre y después del intervalo de tiempo especificado. Por ejemplo, se puede utilizar al intentar establecer una conexión a una Base de datos. Queremos aseguranos que se haga, así que decidimos reintentar entrablar la conexión cada 5s, y estar monitoreando, si en 5 intentos no se ha logrado, puede entonces, que el servidor este caído.
* `setImmediate`. Función similar a setInterval solo que se ejecuta una sola vez, pero al instante.
* `require()`. Se puede utilizar desde cualquier sitio y permite requerir cualquier módulo.
* `export`. Permite exportar módulos para poder utilizarlos desde cualquier lugar.
* `__dirname`. Retorna el path del archivo en el que nos encotramos. Similar a `__filename`.
* `process`.

[Código](./modules/globals.js)


---
### File system <a name="Fs"/>[↑](#inicio)
File system, conocido también como *fs*, provee una API para interactuar con el sistema de archivos cerca del estándar POSIX. POSIX es el estándar para interfaces de comando y shell, las siglas las significan: “Interfaz de sistema operativo portátil”; la X de POSIX es por UNIX.

File system nos permite acceder a archivos de nuestro sistema, leerlos, escribirlos, modificarlos, etc. Es muy útil cuando trabajamos en precompiladores, o cualquier operación que requiera un grabado a disco.
Todo este tipo de procesos se ejecutan de forma asíncrona.

**Todos los métodos tienen una alternativa síncrona pero no es recomendado utilizarla, pues pordría bloquear el event loop con más facilidad, es decir que se detiene el proceso principal, y en caso de querer ejecutar más tareas, estas estarían a la espera de finalizar el proceso en curso.** Por ejemplo readFile tiene su función asíncrona, que es readFileSync.

Algunas de las funciones más útiles son las siguientes:

Función | Descripción
--------|------------
`fs.readFile(ruta, callback)` | Lee el contenido del archivo y lo muestra en buffer, para mostrar el contenido en texto se puede utilizar el método `.toString()`
`fs.writeFile(ruta, contenido, callback)` | Escribe en el archivo el contenido agregado. Si no existe el archivo entonces lo crea. 
`fs.unlink(ruta, callback)` | Elimina un archivo o un '*link simbólico*'.

[Código](./modules/fs.js)


---
### Console <a name="Console"/>[↑](#inicio)
El módulo Console de Node, nos permite impirmir lo que sea en la consola de nuestro proyecto. Tiene muchas funciones que son muy útiles para entender que esta pasando en nuestro código, cuantas veces sucede algo y visualizar de forma eficaz multiples valores y variables.

Función | Descripción
--------|------------
`console.log()` | Simplemente permite visualizar lo recibido en la consola.
`console.info()` | Funciona igual que el `console.log()`, a diferencia de que cuando se tengan plugins o módulos que puedan sobreescribir la consola por defecto, se pueden tener logs para cosas que nos interese guardar o información que nos sea relevante
`console.error()` | Al igual que `console.info()`, pero es utilizado para guardar errores.
`console.warn()` | Al igual que `console.info()`, pero es utilizado para guardar advertencias de las que haya que estar al pendiente.
`console.table()` | Muestra los datos en forma de módulos o tablas, muy útil para mostrar ordenadamente el contenido de arreglos.
`console.group()` | Nos permite "agrupar" un montón de logs para especificar que forman parte de una sola cosa, como que pertenecen a una funcionalidad o módulo específico.
`console.count()` | Contadores. Nos cuenta las veces que se repite el console, mostrando el resultado en la consola. `console.countReset`, resetea la cuenta del `console.count`.

[Código](./modules/console.js)


---
### Try / Catch <a name="TryCatch"/>[↑](#inicio)
**¿Cómo funcionan los errores en Node.js?** Cuando sucede un error durante la ejecución de nuestro código, Node por defecto terminará completamente la ejecución de nuestro código para avisarnos que ha ocurrido un error, esto es debido a que los errores se notifican por hilos, por lo tanto si el error sucede en el hilo principal del *event loop*, ese es el hilo que se "rompera", si el error ocurre en alguna *función asíncrona* el hilo que se "rompe" es el de esa función. Independientemente del hilo donde susceda el error todo nuestro proyecto se suspenderá.

Con Node podemos manejar los errores de una manera muy optima para evitar que se detengan nuestros procesos, para eso se utiliza **try/catch**. Siendo **try** el bloque de código que ae intentará ejecutar estando en "observación" de alguna posible falla y **catch** la función que atrapará el error; es aquí donde se específica que pasará con el.

Por lo tanto, cuando sabemos que algo puede fallar, simplemente basta con encerrarlo dentro de un *try/catch* y ejecutarlo.

[Código](./modules/errors.js)


---
### Procesos Hijo <a name="Procesos-hijo"/>[↑](#inicio)
Un **proceso** simplemente es algo que se ejecuta y termina, como el proceso de Node que se tiene, simplemente termina, de este proceso podemos acceder a la variable `.process`, por ejemplo, para obtener el `.process.env`. 

Node.js permite levantar otros procesos, además de este, puede ser con Node, con Python, con Variables del sistema, con scripts, etc. Para esto, dentro del core hay una librería, un módulo, que se llama **child_process**. `child_process` tiene dos funciones spawn y exec, mediante las cuales podemos iniciar un proceso secundario para ejecutar otros programas en el sistema.

* `exec` de `child_process` permite ejecutar múltiples procesos "uno por debajo de otro", lo cuál nos deja seguir haciendo cosas desde nuestro código y trabajar procesos por debajo. Por ejemplo, imagenemos que tenemos un código hecho en Python diseñado para resolver un problema en específico, y no queremos migrarlo a Node, porque este script funciona muy bien en Python, pero queremos utilizar su resultado dentro de nuestro código Node, pues simplemente con `exec` podemos traer ese código y comenzar a trabajar con él. Podemos llamar a `exec` para ejecuciones sencillas.

* `spawn` de `child_process` permite invocar un proceso nuevo de Node.js y comenzar a ver que ocurre con ese proceso. La ventaja de este enfoque es que obtienes mayor control del proceso, y del estado en el que se encuenta.

La diferencia más significativa entre `child_process.spawn` y `child_process.exec` está en que *spawn devuelve un stream* y *exec devuelve un buffer*.
* Usar `spawn` cuando el proceso hijo devuelva datos binarios enormes a Node.
* Usar `exec` cuando el proceso hijo devuelva mensajes de estado simples.
* Usar `spawn` cuando se quieran recibir datos desde que el proceso arranca.
* Usar `exec` cuando se quieran recibir datos al final de la ejecución.

[Código](./modules/childProcess.js)

### Documentación:
* [Diferencia entre spawn y exec de child_process de NodeJS](https://yosoy.dev/diferencia-entre-spawn-y-exec-de-child_process-de-nodejs/)


---
### Módulos nativos en C++ <a name="C++"/>[↑](#inicio)
**¿Cómo trabajar con módulos nativos de C++ desde JavaScript?** Se deben seguir la siguiente serie de pasos para poder compilar módulos de *c++* en Node.js:
* Instalar node-gyp de forma global. Para eso, ejecutar el comando: `npm i -g node-gyp`.
* Crear archivo fuente. Un ejemplo se puede encontrar en la [documentación de node](https://nodejs.org/api/addons.html#addons_hello_world).
* Crea un *binding.gyp* en la raíz del módulo, con el siguiente contenido:
      {
        "targets": [
          {
            "target_name": "addon",
            "sources": [ "hello.cc" ]
          }
        ]
      }
* En la carpeta raíz del módulo, ejecutar el comando: `node-gyp configure`. Esto generará un directorio *build*. **NOTA:** node-gyp requiere Python 2 para funcionar. 
* En la carpeta raiz del módulo, ejecutar el comando: `node-gyp build`. El módulo se compilará y se podrá importar en JavaScript. Se puede validar que exista el archivo *build/Release/addon.node* (es un binario, así que no se podrá abrir).
* Para usarlo, crear un archivo js, importar el módulo: `const addon = require('./build/Release/addon');` y llamarlo: `addon.hola()`. Para el ejemplo debería imprimir: *mundo*

[Código](./modules/natives)


---
### HTTP <a name="HTTP"/>[↑](#inicio)
Este módulo permite crear un servidor o conectarnos con servidores externos directamente desde Node.js sin tener que pasar por ningún otro sitio.

Para crear un servidor se utiliza el método `http.createServer(function(req, res) {}`, donde los párametros *request* y *response*, hacen referencia a las peticiones que hace el cliente al servidor, donde se incluyen cabeceras, métodos, etc, y la respuesta que el servidor envía al cliente. Ambos parámetros tienen sus métodos propios. 

Los servidores creados desde este módulo pueden escuchar desde cualquier puerto del sistema, algunos de estos puertos ya son reservados, desde luego, podríamos utilizarlos si accedemos como administrador, sin embargo, iniciar Node.js como administrador no suele ser buena idea a nivel de seguridad, especialmente si se trabajará con un servidor donde puede acceder gente desde fuera ya que podrían tener acceso de superadministrador en toda la máquina debido a algún problema producido por una escala de privilegios, ya sea por fallo de nuestro código, algún bug nativo desconocido o algún problema de seguridad.

[Código](./modules/http.js)


---
### Sistema Operativo (Operative System (OS)) <a name="OS"/>[↑](#inicio)
Este módulo permite acceder, desde JavaScript, a toda la información de nuestro dispositivo y nuestro sistema operativo que suele estar disponible solo desde lenguajes de muy bajo nivel. Permite acceder a la memoría RAM, el número de cores del núcleo, el tipo de sistema de archivos, etc.

En resumen, el módulo de Sistema Operativo permite acceder a información que suelen ser de muy bajo nivel pero que realmente es demasiado útil.

Algunas de las funciones a las que podemos acceder desde este módulo son:

Método | Descripción
-------|------------
`os.arch()` | Muestra la arquitectura del equipo. Esto es importante si por ejemplo se quiere trabajar con *módulos en C* o *módulos nativos* donde es necesario realizar alguna compilación específica siguiendo cierto *proceso hijo* donde se requiera utilizar un script de compilación u otro dependiendo de la arquitectura del sistema.
`os.platform()` | Nos muestra el sistema opertativo sobre el que estamos corriendo el código, por ejemplo si estamos en Linux, Windows, Android, etc. Esto es importante si se requiere ejecutar comandos directos a terminal, por ejemplo `ls` que en Linux esta bien, pero en Windows es `dir`. También es útil para tener estadísticas, por ejemplo, para saber en que OS se esta utilizando más nuestro código.
`os.cpus()` | Permite acceder a la información de las CPUs que tenemos dentro de nuestro sistema. Útil para saber cuántos cores tiene nuestro procesador (`os.cpus().length`) para así saber cuántos procesos de Node.js independientes pueden estar corriendo si se ejecutan uno por núcleo.
`os.constants` | Muestra todos los errores y todas las señales del sistema.
`os.freemem()` | Nos muestra en bytes la memoria libre que tiene el sistema. Esto puede ser muy útil cuando queremos hacer una operación muy grande y sabemos que consumirá mucho espacio en memoria, así podremos validar si contamos con espacio suficiente para su ejecución o antes de hacer la operación preferemos suspenderla para evitar colapsar nuestro sistema.
`os.totalmem()` | Nos muestra en bytes la memoria disponible que tiene el sistema.
`os.homedir()` | Permite saber cuál es el directorio raíz del usuario.
`os.tmpdir()` | Permite saber cuál es la ubicación del directorio temporal.
`os.hostname()` | Muestra cuál es el hostname del sistema.
`os.networkInterfaces()` | Nos muestra todas las interfaces de red activas en el dispositivo. 

[Código](./modules/os.js)


---
### Process <a name="Process"/>[↑](#inicio)
El Proceso (***process***) de Node, viene dentro de los *módulos globales*, por lo tanto, para utilizarlo desde cualquier parte, no es necesario requerirlo.

El objecto process es una instancia de *EventEmitter*; podemos suscribirnos a el para escuchar eventos de node, algunos de estos eventos son:

Método | Descripción
-------|------------
`process.on('exit', () => {})` | Se ejecuta cuando Node detiene el *eventloop* y cierra su proceso principal. Una vez que se ejectue el `exit`, es importante tener en mente que a partir de aquí todo lo que se ejecute tiene que ser síncrono y se ejecutará en el hilo principal, ya que se ha realizado una desconexión al resto de hilos.
`process.on('beforeExit', () => {})` | Se ejecuta justo antes de que termine nuestro proceso, antes de que se desconectonecten los beans y módulos.
`process.on('uncaughtException', () => {} ` | Captura una excepción cuando nadie más la ha capturado, por falta de algún Try/Catch. Muy útil cuando queremos asegurarnos de que todas las cosas en nuestro proceso funcionan bien o para monitorizar algún proceso en producción.
`process.on('uncaughtRejection', () => {} ` | Captura promesas rechazadas que nadie más las ha capturado, por falta de algún Catch.

En resumen `process.on()` permite escuchar diferentes "señales" durante la ejecución de nuestro código, por ejemplo, cuando hay algún nuevo mensaje `process.on('message', () => {}` o cuando se abre un nuevo listener `process.on('newListener', () => {}`, cuando hay una excepción que si se haya manejado o una que no, etc.

[Código](./modules/process.js)


---
## Módulos y paquetes externos <a name="ModulosExternos"/>[↑](#inicio)
### Gestión de paquetes: NPM y package.json <a name="NPM"/>[↑](#inicio)
**NPM** es un gestor de paquetes, básicamente permite tener paquetes (fuera de los módulos del Core de Node) para prácticamente todo, por ejemplo, paquetes para saber si un número es impar (*is-odd*), inclusive para determinar si un número en verdad es un número. Utilizar paquetes como estos dos ejemplos, suele ser muy útil, ya que ahí se hacen validadiciones que seguramente ni siquiera se nos pudieron haber ocurrido, porque hasta que no llegamos al caso de uso en el que se nos rompe el proceso, es hasta cuando nos percatamos de estas posibles fallas. Esta práctica es muy común en Node pero al mismo tiempo puede ser un problema, ya que se tiende a tener muchisímas dependencias en nuestro código, esto parece incierto, pero no lo es; simplemente el paquete *is-odd*, que solo sirve para determinar si un número es impar, ya necesita la dependecia *is-number*; otro ejemplo es el módulo de *express* que tiene 30 dependecias, y estas a su vez pueden tener más dependencias. Esto añade riesgos de seguridad o fallos debido a faltas de actualización y a su vez, esto puede afectar de manera directa y considerable a nuestro código.

Una buena práctica al utilizar paquetes de NPM es revisar las estadísticas de cada paquete que se quiera instalar, es bueno revisar que los paquetes esten bien y actualizados, por ejemplo:
* revisar que no este en alguna versión 0.X.X, ya que las versiones 0 suelen ser beta,
* que la última publicación no haya sido hace muchos meses, 
* que no se tengan muchos *open issues* o *pull requests* abiertas, o
* incluso el número de descargas mensuales o semanales.

Para utilizar correctamente *npm* en un proyecto, es necesario inicializarlo, corriendo el comando `npm init` desde la carpeta raíz del proyecto, este comando creará el archivo ***package.json***, el cuál incluye información como el *nombre del paquete*, la *versión*, una *descripción*, el *punto de entrada*, su *repositorio*, el *autor*, las *dependencias*, etc. En resumen, es toda la información de nuestro paquete.

Para instalar un módulo o dependencia en nuestro proyecto el comando a utilizar es: `npm install 'nombre-del-paquete'` o `npm i 'nombre-del-paquete'`, se pueden incluir modificadores como `-g` que indica que ese paquete se instalará de manera global, es decir, se instalará en todo nuestro dispositivo, o el parametro `-D` que indica que el paquete o módulo se instalará solo para el entorno de Desarrollo. Los paquetes instalados se añaden en el apartado *"dependencies"* del *package.json*. 

**NOTA:** Cuando trabajamos sobre un proyecto que ya ha sido creado, es necesario ejecutar el comando *npm install* en la raíz del mismo (o en su defecto, en la carpeta donde se encuentra el archivo *package.json*), para instalar las dependencias de dicho proyecto.

Para utilizar los paquetes instalados basta con hacer un *require* del mismo, ejemplo: `const isOdd = require('is-odd');`

[Código](./packages/npm)

### Documentación:
[Web Oficial](https://www.npmjs.com/)


---
### Construyendo módulos: Require e Import <a name="Module"/>[↑](#inicio)
**¿Cómo trabajar con módulos propios?** Para poder utilizar bloques de código (módulos) en otras partes de nuestro código, después de crearlos, es necesario exportarlos, una manera de hacerlo es utilizando la palabra clave de Node.js `module.exports`, de la siguiente manera: `module.exports = saludar;` ya que si no se exporta nada no hay ninguna funcionalidad a la que se pueda acceder. Después, para utilizar estos módulos en otras partes del código, debemos "traerlos", importarlos. Una manera de hacerlo es utilizando la *palabra clave* `require` de Node.js, como en el siguiente ejemplo: `const modulo = require('./module');`. Dicho módulo debe ser "requerido" desde su ubicación (ruta) en el proyecto. Esta es la sintaxis de ***requireJS*** que viene en Node.js desde sus primeras versiones.

Node.js nos permite exportar módulos "complejos" que tengan diferentes funcionalidades, opciones o formas, esto, desde un objeto, por ejemplo:

    module.exports = {
      saludar,
      prop1: 'Hola que tal!!',
      prop2: 'Hi!!'
    }

Con ***EcmaScript 6***, se tiene una sintaxis nueva, donde para exportar se hace con las palabras clave `export default`, por ejemplo: `export default saludar`, mientras que para importar las palabras clave que se utilzan son `import` y `from`, por ejemplo: `import modulo from './modulo.mjs'`, donde al igual que en *requireJS*, los modulos deberán ser importados desde su ruta en el proyecto. Actualmente utilizar esta sintaxis no es ningún problema, pero en versiones de Node anteriores a la *v12.22.1* era necesario ejecutar los proyectos como si fueran "experimentales", utilizando el comando `node --experimental-module index.mjs`, en lugar de solo `node index.mjs`.

La documentación de V8 recomienda utilizar la extensión ***.mjs*** por las siguientes razones:
* Es bueno por claridad, es decir, deja claro qué archivos son módulos y cuáles JavaScript.
* Asegura que tus archivos de módulo sean analizados como un módulo por los entornos de ejecución como Node.js y herramientas de compilación como Babel.

[Código](./packages/module)


---
### Módulos útiles <a name="Modulos-utiles"/>[↑](#inicio)
Algunos de los módulos más utilizados son:

#### bcrypt
Es un módulo que funciona sobre crypt, pero que añade múltiples útilidades y funciones que permiten cifrar y comparar los resultados.

Entre sus principales funciones para el cifrado y la comparación de contraseñas, estan:

* `bcrypt.hash()`. Encripta un texto y recibe tres parámetros:
  * el texto a encriptar; 
  * el número de rondas que se daran para hacer el hash, un hash simplemente es aplicar un algoritmo (operaciones matemáticas y/o criptológicas) para poder cambiar el texto inicial por una cadena de caracteres sin sentido y
  * una función callback, para obtener el resultado o determinar si hay algún error.
* `bcrypt.compare()`. Compara un hash y un texto, para ver si tienen sentido, a nivel interno se encarga de determinar si el texto es capaz de generar el hash, o no. Al igual que *hash* también recibe tres parámetros, el texto o contraseña a comparar, el hash de referencia y una función callback.

bcrypt también funciona con promesas, no solo con callbacks.

[Código](./packages/utiles/bcrypt.js)

#### moment
La librería moment nos facilita trabajar con fechas, nos permite, por ejemplo, determinar cuánto tiempo ha pasado desde una determinada fecha, o cuanto tiempo queda para la siguiente, etc.

El crear una fecha desde *moment* no solo implica tener la fecha actual, sino todas las útilidades del módulo *moment* en esa misma variable.

Para crear una varaible moment basta con inicializarla a partir de la constante importada, `let ahora = moment();` donde parte de su utilería sería:
* `ahora.toString()`. Funciona igual a como si en JavaScript obtuvieramos una fecha: *Sun Apr 18 2021 22:58:29 GMT-0500*.
* `ahora.format('YYYY/MM/DD - HH:mm')`. Da un formato especifíco a la fecha; como parámetro se pasa el formato deseado.

[Código](./packages/utiles/moment.js)

#### sharp
Trabaja con una librería de manejo de imágenes en C++, que nos permite procesar imágenes.

Para utilizarla basta con agregar la ruta de la imagen a procesar como un parámetro y en seguida agregar los métodos que se quieran procesar, por ejemplo:
* `.resize(tamaño-en-número-de-pixeles)`, para cambiar el tamaño de la imagen. El tamaño se define desde parámetro y se agrega en pixeles, o
* `.grayscale()`, para cambiar la imagen a una en blanco y negro.

Después de añadir los métodos del procesamiento de la imagen se debe especificar que se desea hacer con estos cambios, para crear una imagen el método a ejecutar debe ser `.toFile('nombre-de-la-nueva-imagen')`

[Código](./packages/utiles/sharp.js)


---
### Datos almacenados vs en memoria (Buffer y Stream) <a name="Datos"/>[↑](#inicio)
**¿Porque hay datos que se manejan "al vuelo" y datos que no?** Todo el manejo de Datos funciona por tiempos, escribir en memoria es demasiado rápido, porque la CPU y la memoria RAM cuentan con protocolos de comunicación mucho muy rápidos, sin embargo cuando se quiere escribir en Disco los procesos son mucho más lentos, porque el Disco esta diseñado para manejar un gran volumen de archivos 1, 2TB, a diferencia de la memoria RAM que puede tener 4, 8, 16GB y esta diseñada para leer y escribir archivos de forma super rápida. Los datos en memoria se destruyen una vez que dejan de ser útiles, por otro lado, en los Discos duros persisten todo el tiempo. Una de las desventajas de los Discos, a pesar de su gran capacidad de almacenamiento, es la lentidud en tiempos durante las operaciones de lectura y escritura, especialmente los Discos Magnéticos.

Una de las estrategías a seguir para reducir el trabajo en Disco, para casos en los que se requiera, es traspasar la información (data) leída a través de memoria, de función en función hasta ejecutar todo nuestro proceso y una vez que termine, si se necesita escribir en disco, se escribe, para así evitar el estar leyendo y escribiendo en Disco dicha data. Esto permite que el procesamiento de archivos o información sea mucho más rápido.

Estas son las bases para entender los ***buffer*** y los ***streams***. Un *buffer* es un montón de datos y un *stream* es un proceso donde pasan un montón de datos.

#### Buffers
Un ***buffer*** no es más que datos en binario, datos crudos, que van y vienen de un lado a otro. Un ejemplo de un buffer se puede observar al trabajar con ficheros de *fileSystem (fs)*, donde el fichero se puede observar en "números raros" (**bytes**) que no se pueden decodificar hasta agregar algún método como `.toString()`.

En Node.js, los objetos Buffer se usan para representar datos binarios en forma de una secuencia de bytes. Es por ello que la palabra *'Hola'* equivale en hexadecimal `48 6f 6c 61`.
Muchas API de Node.js, por ejemplo, los flujos y las operaciones del sistema de archivos, admiten Buffers, ya que las interacciones con el sistema operativo u otros procesos generalmente siempre ocurren en términos de datos binarios.

Un buffer nos permite:
* trabajar con datos en su versión más cruda, sin generar tipos y sin ser procesados, simplemente Data y
* trabajar la información posición a posición, dato a dato, paquete a paquete, o byte a byte, tal cuál como un arreglo.

Un buffer suele ser la salida de un stream.

En Node.js se tiene la clase **`Buffer`** que permite trabajar con buffers, por ejemplo crearlos, con el método `.alloc()` o el método `.from()` o leerlos como texto plano, con el método `.toString()`.

[Código](./memory/buffer.js)

#### Streams
Se puede decir que los ***stream*** simplemente son el paso de datos entre un punto y otro. Hay tres tipos de stream:
* *streams de lectura*. Donde se tiene un origen y el stream se encarga de recoger los datos y arrojarlos hacia donde se desee para trabajar con ellos.
* *streams de escritura*. Donde se tiene un destino, el stream se encarga de arrojarle datos, y este se encarga de trabajar con ellos.
* *streams de doble sentido*. Donde se puede leer y escribir datos y trabajar con ellos.

El procesamiento de *streams* se administra en paquetes de *buffers* (***chunks*** de datos) de un tamaño dependiente a la configuración de memoria, que a su vez depende del sistema operativo y de lo que pueda hacer Node por detrás.

* **Streams de lectura**. 
Podemos decodificar un stream utilizando el método `.on('data', (chunk) => {} )` de diferentes maneras:
  * (1) aplicando el método `.toString()` de buffers al *chunk*. Pero esta no es la mejor opción, si conocemos la decodificación del archivo que vamos a leer podemos utilizar:
  * (2) el método `.setEncoding('UTF8');` especificando el "encoding". **UTF8** es el estándar internacional que permite escribir caracteres, tildes, 'ñ', cualquier letra que no sea tipica del alfabeto estadounidense, etc.
  * (3) o para el caso de archivos muy grandes que requieran de múltiples *chunks*, podemos concatenarlos en una sola variable (`data += chunk`) y especificar cuando hemos terminado con el método `.on('end', (chunk) => {} )` y dentro de su callback trabajar con lo que deseemos a partir del stream procesado. 

La diferencia entre estos métodos es notable en memoria cuando se trabaja con archivos muy grandes. Para cosas pequeñas, que de antemano se sabe que no habrá problema, tal vez lo mejor sea ni siquiera utilizar un stream, hace sentido utilizarlo cuando por ejemplo esperamos una petición HTTP y no sabemos exactamente su tamaño y contenido.

* **Streams de escritura**.
Para crear un stream de escritura se puede trabajar directamente en la salida estándar del sistema de la siguiente manera, `process.stdout.write('Hola');`, con esta línea se escribe directamente en el buffer de la salida del sistema.

* **Streams de doble sentido**.
Podemos recibir un dato, modificarlo y escribirlo, creando un buffer de transformación que por ejemplo, lea de un fichero, transforme esa data a mayuscúlas y después escriba en la salida estándar del sistema, para esto, creamos un `stream.Transform` que utilizará una función `Mayus` para transformar la lectura en mayuscúlas;
 después "construimos" un stream Transform heredando de Mayus y utilizamos su método `.prototype` sobre `_transform` para convertir el `chunk` (paquete de datos) a mayuscúlas y el resultado lo "envíamos" como respuesta. Con esto ya tendríamos nuestra transformación pero para poderla ver aún necesita ser creada `let mayus = new Mayus();` y sobre de nuestro "readStream" creado a partir del archivo con el texto de entrada concatenamos los métodos `.pipe()` utilizando como parámetros `mayus` y después `process.stdout`, para transformar nuestra data de entrada a mayuscúlas y mostrarla directamente en nuestro buffer de la salida del sistema.

[Código](./memory/stream.js)


---
## "Trucos" jeje <a name="Trucos"/>[↑](#inicio)
### Temporizador de procesos (*Benchmark*) <a name="Benchmark"/>[↑](#inicio)
Podemos conocer exactamente cuánto tiempo real tarda en ejecutarse algún proceso o función desde su inicio hasta su fin a partir del método `time()` del módulo `console`. Esto es muy útil para comparar tiempos entre algoritmos y así poder mejorar el rendimiento de nuestras aplicaciones.

Para utilizar este método se debe "inicializar" el temporizador con el método `console.time('NomabreDelTemporizador')` y para marcar el fin del proceso, o del tiempo a contar se agrega `console.timeEnd('NomabreDelTemporizador')`.

**Esta técina funciona de la misma manera con funciones asíncronas.**

[Código](./tricks/benchmarking.js)

### Debugger <a name="Debugger"/>[↑](#inicio)
Node.js viene integrado con un modo de *debug* para poder conectarnos desde cualquier herramienta de inspección de código a nuestro código en Node.js, para eso solo se debe desplegar con un *flag*.

Si queremos correr nuestra aplicación con el debugger, debemos seguir los siguientes pasos:
* Agregar el *flag* `--inspect` al comando de ejecución después del "programa" `node`, por ejemplo: `node --inspect modules/http.js`. Esto permite que además de abrirse todo lo requerido en la aplicación de Node.js, se abra un segundo puerto con el '***inspector***'. Después de correr el comando, podemos ver este otro proceso en la consola como: `Debugger listening on ws:127.0.0.1:9229/...`.
* Una de las formas más rápida para inspeccionar es desde Chrome, para abrir este inspector es necesario ingresar a la ruta *chrome://inspect* en el navegador Google Chrome. Al hacerlo se podrá visualizar el *"Target"* con su ubicación dentro del equipo.
* Después para abrir el inspector, basta con dar click en el link ***inspect***.

Dentro del ***Inspect*** se presentan diferentes herramientas además de la **Console** como:
* **Memory** la cuál nos permite observar en tiempo real como esta funcionando la memoria y monitorear los procesos de la aplicación. Además se muestran todas las opciones de memoria que se pueden tener.
* **Profiler** permite "cargar" un perfil de CPU para después ejecutar procesos de la aplicación y grabarlos. Desde las grabaciones podemos ver los tiempos que se ha tomado en realizar las diferentes funciones, es aquí donde podemos estar al pendiente de operaciones que tomen mucho tiempo en ejecutarse.
* **Sources** aquí es donde podemos debuggear el código de la aplicación. En la ventana derecha podemos agregar nuestra carpeta de trabajo (workspace folder), al abrir la carpeta, se maracará con un punto verde el archivo sobre el que estamos trabajando. En la ventana de la izquierda se tienen las pestañas:
  * *Watch* donde podemos agregar variables para estar al pendiente de sus "transformaciones" (a la escucha de sus cambios) a través de la ejecución del código.
  * *Breakpoints* muestra los puntos donde el código hará una "pausa". Para agregarlos basta con dar click a la izquierda del número de la línea de nuestro código, al seleccionar los puntos, estos se marcaran en azul.
  * *Scope* aquí podemos ver todos los métodos, variables, etc de los diferentes scopes del proyecto, por ejemplo, todo "el contenido" del scope global, o "el contenido" del bloque en ejecución.
  * *Call Stack* permite ver toda la pila de llamadas que se hayan hecho durante la ejecución del código.

El debugger de Node nos permite hacer todo lo que queramos en nuestro código durante el tiempo de ejecución para poder ver que es lo que esta pasando.

[Código](./modules/http.js)


### Error First Callbacks <a name="ErrorCallbacks"/>[↑](#inicio)
En las funciones *Callback*, por lo regular hay un patrón que se sigue, es el ***Error First Callback***, esto nos permite saber que siempre que tengamos un *Callback*, el primer parámetro debería ser el *error*, esto es básicamente por una convención generada desde la idea principal de que *Todo puede fallar*, en JavaScript esto no es una excepción, siempre debemos estar pendientes de ello y ser capaces de controlarlo, una forma muy sencilla de pasar todos los argumentos deseados en nuestro *Callback*, pero teniendo siempre el *error* en el mismo sitio, es la de saber que el *error* es siempre el primer argumento en pasarse. Otro de los patrones tipicos suele ser tener el Calback como úlitma función en pasarse, aunque depende del caso, por ejemplo en los `setInterval` o en un `setTimeout`, el *Callback* va a ser siempre la primera función.

Siempre que utilizamos Callbacks, lo primero que se tiene que hacer, una vez que se llame a una función, es revisar si esta tiene un error, porque si lo hay deberíamos detener completamente la ejecución de la función, capturar el error o hacer lo que se tenga que hacer, en caso de que se pueda solucionar, se soluciona, de lo contrario terminamos la ejecución de la función, haciendo un `return false`.

Si quisieramos que quien este llamando a la función gestione el error, podríamos intentar lanzar un error (una excepción) con un `throw error`: pero **dentro de un Callback o una función asíncrona esto NO resulta**. Por eso el patrón *Error First Callback* es el que debemos seguir siempre que se trabaje con funciones asíncronas y con Callbacks.  Un `try / catch()` podría tomar el error dentro de estas funciones pero no rodeando a su Callback porque esta funcionando en "otro sitio".

**`throw` solo resulta utilizando funciones síncronas.**

*Error First Callback*, es muy útil para lenguajes asíncronos, especialmente para Node.js o para todo JavaScript.

[Código](./tricks/errorFirst.js)


---
## Manejo de Herramientas <a name="Herramientas"/>[↑](#inicio)
Las herramientas son paquetes de terceros pero construidos integramente en Node y pueden ser utilizados para crear practicamente lo que sea funcionando para muchisímas cosas distintas.

### Scraping (*Puppeteer*)<a name="Scraping"/>[↑](#inicio)
Web scraping es una técnica utilizada mediante programas de software para extraer información de sitios web. Usualmente, estos programas simulan la navegación de un humano en la *World Wide Web* ya sea utilizando el protocolo HTTP manualmente, o incrustando un navegador en una aplicación.

***Puppeteer*** es una libreria de Node la cual provee una API de alto nivel para controlar Chrome o Chromium *"headless"* sobre el protocolo *DevTools*. También puede ser configurado para usar Chorme o Chromium completamente *non-headless*. En otras palabras, parte de lo que puede hacer es levantar un Chrome o Chromium pero directamente sin poder verlo.

Al instalar *puppeteer* por primera vez, se instala *Chromium*, que simplemente es el motor Open Source de Chrome, y sobre él, construyen todo el navegador.

**Todo *puppeteer* funciona de forma asíncrona.** Para poder hacer todas las peticiones asíncronas se pueden utilizar las ***funciones autoejecutables***. Una *Función Autoejecutable* simplemente es una función anonima asíncrona rodeada entre paratesís, para ser convertida en una expresion, y después es ejecutada,

    (async () => {
      // Nuestro Código...
    })();

estas funciones nos permiten utilizar los `await`.

Algunos de los métodos mas básicos que podemos utilizar en *puppeteer* son:

Método | Ejemplo | Descripción
-------|---------|------------
`puppeteer.launch();` | `const browser = await puppeteer.launch();` | Nos lanza o "crea" un Navegador, pero de forma "oculta", para poder visualizarlo se puede agregar la propiedad `headless: false`, por ejemplo, `const browser = await puppeteer.launch({ headless: false });`.
`.close()` | `browser.close();` | Nos cierra el navegador lanzado.
`.newPage()` | `browser.newPage` | Abre una página en el navegador.
`.goto()` | `await page.goto('https://vainilladev.com');` | Metodo de un `newPage()` que permite redirigir a algún sitio en especifíco.
`.evaluate(() => {})` |  `var titulo1 = await page.evaluate(() => {...});` | Metodo de un `newPage()` que ejecuta un script dentro de la página.

*Puppeteer* puede ser útil para ejecutar todo lo que se quiera desde el navegador, por ejemplo abrir una página, recoger la información deseada y podernosla llevar a un fichero, a una Base Datos, etc.

[Código](./tools/scraping)

### Documentación:
[Puppeteer](https://developers.google.com/web/tools/puppeteer)

### Automatización de Procesos <a name="Automatizacion"/>[↑](#inicio)
Existen muchas herramientas de automatización de procesos, como por ejemplo, *Webpack*, que esta más orientada hacía la automatización de procesos de empaquetado de Frontend, o como *Grunt* o *Gulp* que se orientan a casi cualquier cosa.

Una de las cosas utiles que ofrece el ***pacakge.json*** son los *scripts*, esto nos permite crear nuevos *scripts* dentro de nuestro paquete y con esto poder automatizar TODO. Uno de los sitios donde más se utiliza la automatización es en Frontend; cuando se tienen múltiples carpetas y se quiere ir viendo los cambios de algún "frontal", o cuando se cuenta con *css* en *Less* o en *Sass* y se quiere precompliar, o si se quiere pasar todo el código por *Babel*; cualquiera de estas tareas es posible automatizarlas creando *scripts* en ***gulp***.

*Gulp*. Es una herramienta de construcción en JavaScript construida sobre flujos de nodos. Estos flujos facilitan la conexión de operaciones de archivos a través de canalizaciones. *Gulp* lee el sistema de archivos y canaliza los datos disponibles de un complemento de un solo propósito a otro a través del `.pipe()`, haciendo una tarea a la vez. Los archivos originales no se ven afectados hasta que se procesan todos los complementos. Se puede configurar para modificar los archivos originales o para crear nuevos. Esto otorga la capacidad de realizar tareas complejas mediante la vinculación de sus numerosos complementos. Los usuarios también pueden escribir sus propios complementos para definir sus propias tareas.

Para trabajar con *gulp*, es necesario crear un archivo ***gulpfile.js***, este archivo es el que abrirá *gulp* cuando se quiera crear cualquier cosa y revisar que todo funcione. Dentro de este archivo se pueden crear tareas desde el método `gulp.task('taskName', (cb) => {...})`, donde el parámetro *Callback* quiere decir que la tarea puede ser asíncrona y una vez que terminé se ejecuta el *callback* y con esto sabremos que la tarea ha terminado y podremos ejecutar, por ejemplo, el precompilado de *css*, el precompilado de nuevas versiones de JavaScript para que funcione en navegadores viejos, y de más tareas asíncronas que se pueden correr en paralelo.

La manera más sencilla de ejecutar una `gulp.task` es creando un *script* automatizado en el *package.json*, por ejemplo, si la tarea que deseamos correr se llama *build* el *script* deberá ser `"build": "gulp build"`.

El método `gulp.src()`, indica a *gulp* cuál será el "origen" desde que el *script* debe trabajar, de cierta manera este método retorna un *stream* al que se le pueden "encadenar" otros métodos, como el método `.pipe()`.

***gulp-server-livereload***, sirve para levantar un pequeño servidor web, que con las configuraciones correctas, es capaz de refrescar los cambios realizados en nuestro código (`livereload: true`) (similar a como funciona *Nodemon*), o de abrir automáticamente el servidor (`open: true`).

Podemos crear una ***"tarea defecto"*** de gulp (`gulp.task('default', ...)`), que permita a su vez ejecutar distintas tareas a partir del método `gulp.series('taskName1', 'taskName2', ...)`. Para ejecutar esta tarea basta con crear el script: `"start": "gulp"`.

[Código](./tools/automation)

### Aplicaciones de Escritorio <a name="Aplicaciones"/>[↑](#inicio)
Uno de los paquetes más potentes y utilizados de Node.js es ***electron***, el cuál, simplemente permite convertir desarrollos web en aplicaciones de escritorio. 

Para convertir un desarrollo web en una aplicación de escritorio basta con tener la página web y en un archivo JavaScript (para trabajar con Node) requerir o importar la aplicación principal de electron (`app`) y la parte `BrowserWindow`, para crear una ventana principal (*mainWindow*) donde se verá la aplicación. Para esto se puede crear una función que se encargue de crear un `new BrowserWindow()` y que se sette en la ventana principal. Este nuevo *BrowserWindow* puede ser configurado al momento de su creación, pasando propiedades, por ejemplo, de su tamaño (width and height) o de "preferencia" (webPreferences), para por ejemplo, cargar un script justo antes (`preload: ...`). Después de crear el *BrowserWindow* se debe cargar la aplicación en la ventana principal `mainWindow.loadFile('index.html');`. Sí llamamos la función de crear una ventana antes de que ese proceso este listo para crear la ventana, esta nunca se crearía porque estaría siendo llamada antes de poder existir, para evitar esto podemos aprovechar la capacidad de Node.js de escuchar eventos para escuchar cuando el evento este listo, `app.on('ready', createWindow);`.

[Código](./tools/electron)