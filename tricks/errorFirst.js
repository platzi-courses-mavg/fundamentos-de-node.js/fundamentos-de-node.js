function asincrona(callback) {
  setTimeout(() => {
    try {
      let a = 3 + z;
      callback(null, a);
    } catch (error) {
      // callback(error, null);
      callback(error);
    }
  }, 1000);
}

asincrona((err, dato) => {
  if (err) {
    console.error('Tenemos un error...');
    console.error(err);

    // throw err; // NO FUNCIONA EN FUNCIONES ASÍNCRONAS
    return false;
  }

  console.log('Todo cool, la darta es:', dato);
});