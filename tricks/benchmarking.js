console.time('Todo')

// ---
let suma = 0;
console.time('Bucle');
// for (let i = 0; i < 1000000000; i++) {
for (let i = 0; i < 10000000; i++)
  suma += 1;
console.timeEnd('Bucle');

// ---
let suma1 = 0;
console.time('Bucle1');
for (let j = 0; j < 20000000; j++)
  suma1 += 1;
console.timeEnd('Bucle1');

// ---
console.time('Asíncrono');
console.log('Empieza el proceso asíncrono');
asincrona()
  .then(() => {
    console.timeEnd('Asíncrono')
  })
 
function asincrona() {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('Termina el proceso asíncrono');
      resolve();
    });
  });
}


console.timeEnd('Todo')