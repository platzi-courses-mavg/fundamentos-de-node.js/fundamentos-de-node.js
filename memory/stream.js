const fs = require('fs');
const stream = require('stream');
const util = require('util');

// let data = '';

let readableStream = fs.createReadStream(__dirname + '/input.txt');

// ---
// (1)
readableStream.on('data', (chunk) => { // Un chunk es un buffer.
  console.log(chunk.toString());
});

// ---
// (2)
readableStream.setEncoding('UTF8');
readableStream.on('data', (chunk) => {
  console.log(chunk);
});

// ---
// (3)
readableStream.on('data', (chunk) => {
  data += chunk;
});

readableStream.on('end', () => {
  console.log(data);
})


// ---
process.stdout.write('Hola'); // Escribiendo en el buffer de la salida estándar del sistema.
process.stdout.write('que');
process.stdout.write('tal!');

// ---
const Transform = stream.Transform;

function Mayus() {
  Transform.call(this);
}

util.inherits(Mayus, Transform);

Mayus.prototype._transform = function(chunk, codif, cb) {
   chunkMayus = chunk.toString().toUpperCase();
   this.push(chunkMayus);
   cb();
}

var mayus = new Mayus();

readableStream
  .pipe(mayus)
  .pipe(process.stdout);