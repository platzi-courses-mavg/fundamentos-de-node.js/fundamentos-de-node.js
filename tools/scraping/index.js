const puppeteer = require('puppeteer');

(async () => {
  console.log('Lanzando Navegador');
  // const browser = await puppeteer.launch();
  const browser = await puppeteer.launch({ headless: false });

  const page = await browser.newPage();
  await page.goto('https://vainilladev.com');

  var titulo1 = await page.evaluate(() => {
    // El código dentro de este Scope se ejecuta dentro de la página
    const h1 = document.querySelector('h1');
    console.log(h1.innerHTML);
    return h1.innerHTML;
  });

  console.log(titulo1);

  console.log('Cerramos Navegador');
  browser.close();
  console.log('Navegador cerrador');
})();