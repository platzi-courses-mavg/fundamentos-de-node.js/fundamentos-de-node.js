console.log('Hola Mundo');


let i = 0;

// setInterval recibe dos argumentos, una función que se ejecutará cada cierto tiempo, y el intervalo de ejecuión en milisegundos
setInterval(function() {
  console.log(i);
  // console.log('Sigo activo');
  i++;
  // if (i === 5) 
  //   var a = 3 + z; // Forzamos un error al no declarar la variable z para así detener el proceso
}, 1000) 

console.log('Segunda Instrucción'); 
/* Aquí se puede observar el funcionamiento del Event Loop, donde primero se ejecutan el console.log('Hola Mundo) 
 después la función del setInterval, pero es enviada al Thread Pool, debido a que es un proceso que no se puede ejecutar
 inmediatamente, después se corre el console.log('Segunda Instrucción) para por último ejecutar ahora si la función del
 setInterval */